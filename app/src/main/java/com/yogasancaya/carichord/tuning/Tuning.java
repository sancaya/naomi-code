package com.yogasancaya.carichord.tuning;

public interface Tuning {

    Note[] getNotes();

    Note findNote(String name);
}