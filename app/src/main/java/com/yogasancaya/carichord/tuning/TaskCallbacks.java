package com.yogasancaya.carichord.tuning;

import com.yogasancaya.carichord.models.PitchDifference;

public interface TaskCallbacks {
    void onProgressUpdate(PitchDifference percent);
}
