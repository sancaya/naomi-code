package com.yogasancaya.carichord.tuning.instrument;

import com.yogasancaya.carichord.exception.NoteName;
import com.yogasancaya.carichord.tuning.Note;
import com.yogasancaya.carichord.tuning.Tuning;

import static com.yogasancaya.carichord.exception.NoteName.*;

public class UkuleleDTuning implements Tuning {

    @Override
    public Note[] getNotes() {
        return Pitch.values();
    }

    @Override
    public Note findNote(String name) {
        return Pitch.valueOf(name);
    }

    private enum Pitch implements Note {

        A4(A, 4),
        D4(D, 4),
        F4_SHARP(F, 4, "#"),
        B4(B, 4);

        private final String sign;
        private final int octave;
        private NoteName name;

        Pitch(NoteName name, int octave, String sign) {
            this.name = name;
            this.octave = octave;
            this.sign = sign;
        }

        Pitch(NoteName name, int octave) {
            this.name = name;
            this.octave = octave;
            this.sign = "";
        }

        public NoteName getName() {
            return name;
        }

        @Override
        public int getOctave() {
            return octave;
        }

        @Override
        public String getSign() {
            return sign;
        }
    }
}
