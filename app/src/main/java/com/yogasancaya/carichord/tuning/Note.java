package com.yogasancaya.carichord.tuning;

import com.yogasancaya.carichord.exception.NoteName;

public interface Note {
    NoteName getName();

    int getOctave();

    String getSign();
}
