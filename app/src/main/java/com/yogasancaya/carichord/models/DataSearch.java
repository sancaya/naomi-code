package com.yogasancaya.carichord.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DataSearch {
    public int total;
    public int page;
    public int totalpage;
    public List<DataSearchResult> results;

    public DataSearch(){

    }
    public DataSearch(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            this.total = jsonObject.getInt("total");
            this.page = jsonObject.getInt("page");
            this.totalpage = jsonObject.getInt("totalpage");

            results = new ArrayList<>();
            if(this.total<=0){
                return;
            }
            JSONArray jRes = jsonObject.getJSONArray("result");

            for (int a = 0; a<jRes.length();a++) {
                JSONObject r = jRes.getJSONObject(a);

                DataSearchResult result = new DataSearchResult();
                result.setArtis(r.getString("artis"));
                result.setJudul(r.getString("judul"));
                result.setLagu(r.getString("lagu"));
                result.setArtisurl(r.getString("artisurl"));
                result.setDate(r.getString("date"));
                result.setView(r.getString("view"));

                results.add(result);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}


