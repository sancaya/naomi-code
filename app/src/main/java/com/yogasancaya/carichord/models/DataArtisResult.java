package com.yogasancaya.carichord.models;

public class DataArtisResult {
    String jumlahlagu;
    String artis;
    String artisurl;

    public DataArtisResult(){

    }

    public String getArtis() {
        return artis;
    }

    public String getArtisurl() {
        return artisurl;
    }

    public String getJumlahlagu() {
        return jumlahlagu;
    }

    public String getJumlahChord(){
        return jumlahlagu+" Chord";
    }

    public void setArtis(String artis) {
        this.artis = artis;
    }

    public void setArtisurl(String artisurl) {
        this.artisurl = artisurl;
    }

    public void setJumlahlagu(String jumlahlagu) {
        this.jumlahlagu = jumlahlagu;
    }
}
