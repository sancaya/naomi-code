package com.yogasancaya.carichord.models.audiostream;

import android.util.Log;

import com.yogasancaya.carichord.models.DataSearchResult;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class Data {
    public String Status;
    public int StatusCode;
    public String Title;
    public String DownloadSize;
    public String VideoThumbnail;
    public String FullVideoLink;
    public String Downloadurl;
    public Data(){

    }

    public Data(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            this.Status = jsonObject.getString("Status");
            this.StatusCode = jsonObject.getInt("Status_Code");
            this.DownloadSize = jsonObject.getString("Download_Size");
            this.VideoThumbnail = jsonObject.getString("Video_Thumbnail");
            this.FullVideoLink = jsonObject.getString("Full_Video_Link");
            this.Downloadurl = jsonObject.getString("Download_url");
            this.Title = jsonObject.getString("Title");

        } catch (JSONException e) {
            Log.d("TAG", "DataSearch: "+e.getMessage());
            e.printStackTrace();
        }
    }


}
