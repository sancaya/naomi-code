package com.yogasancaya.carichord.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SingleArtisData {
    public int total;
    public int page;
    public int totalpage;
    public List<SingleArtisDataResult> results;

    public SingleArtisData(){

    }
    public SingleArtisData(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            this.total = jsonObject.getInt("total");
            this.page = jsonObject.getInt("page");
            this.totalpage = jsonObject.getInt("totalpage");

            results = new ArrayList<>();
            if(this.total<=0){
                return;
            }
            JSONArray jRes = jsonObject.getJSONArray("result");

            for (int a = 0; a<jRes.length();a++) {
                JSONObject r = jRes.getJSONObject(a);

                SingleArtisDataResult result = new SingleArtisDataResult();

                result.setArtis(r.getString("artis"));
                result.setJudul(r.getString("judul"));
                result.setLagu(r.getString("lagu"));
                result.setArtisurl(r.getString("artisurl"));
                result.setDate(r.getString("date"));

                results.add(result);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
