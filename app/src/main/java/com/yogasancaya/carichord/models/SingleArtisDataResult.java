package com.yogasancaya.carichord.models;

public class SingleArtisDataResult {
    public String artis;
    public String judul;
    public String lagu;
    public String artisurl;
    public String date;
    public SingleArtisDataResult(){

    }

    public void setArtisurl(String artisurl) {
        this.artisurl = artisurl;
    }

    public void setArtis(String artis) {
        this.artis = artis;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public void setLagu(String lagu) {
        this.lagu = lagu;
    }
}
