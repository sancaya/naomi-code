package com.yogasancaya.carichord.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class DataArtist {
    public int total;
    public int page;
    public int totalpage;
    public List<DataArtisResult> results;

    public DataArtist(){

    }
    public DataArtist(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);
            this.total = jsonObject.getInt("total");
            this.page = jsonObject.getInt("page");
            this.totalpage = jsonObject.getInt("totalpage");

            results = new ArrayList<>();
            if(this.total<=0){
                return;
            }
            JSONArray jRes = jsonObject.getJSONArray("result");

            for (int a = 0; a<jRes.length();a++) {
                JSONObject r = jRes.getJSONObject(a);

                DataArtisResult result = new DataArtisResult();
                result.setJumlahlagu(r.getString("jumlahlagu"));
                result.setArtis(r.getString("artis"));
                result.setArtisurl(r.getString("artisurl"));

                results.add(result);

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
