package com.yogasancaya.carichord.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Lagu {
    public String lagu;
    public String artis;
    public String chords;
    public String date;
    public String judul;
    public String view;
    public String link_youtube;
    public String judul_youtube;
    public String img;
    public String keyword;
    public String artisurl;
    public String notartist;

    public Lagu(){

    }
    public Lagu(String json){
        try {
            JSONObject jsonObject = new JSONObject(json);

//            JSONArray jsona = new JSONArray(json);
//            for (int a = 0; a<jsona.length();a++) {
//                JSONObject jsonObject = jsona.getJSONObject(a);
                this.lagu = jsonObject.getString("lagu");
                this.artis = jsonObject.getString("artis");
                this.chords = jsonObject.getString("chords");
                this.date = jsonObject.getString("date");
                this.judul = jsonObject.getString("judul");
                this.view = jsonObject.getString("view");
                this.link_youtube = jsonObject.getString("linkyt");
                this.judul_youtube = jsonObject.getString("nameyt");
//            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
