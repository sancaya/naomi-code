package com.yogasancaya.carichord.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.yogasancaya.carichord.tuning.Note;
import com.yogasancaya.carichord.tuning.Tuning;
import com.yogasancaya.carichord.ui.tuner.TunerFragment;

public class PitchDifference implements Parcelable {

    public static final Creator<PitchDifference> CREATOR = new Creator<PitchDifference>() {
        public PitchDifference createFromParcel(Parcel in) {
            return new PitchDifference(in);
        }

        public PitchDifference[] newArray(int size) {
            return new PitchDifference[size];
        }
    };

    public final Note closest;
    public final double deviation;

    public PitchDifference(Note closest, double deviation) {
        this.closest = closest;
        this.deviation = deviation;
    }

    private PitchDifference(Parcel in) {
        Tuning tuning = TunerFragment.getCurrentTuning();
        closest = tuning.findNote(in.readString());
        deviation = in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(closest.getName().name());
        dest.writeDouble(deviation);
    }
}
