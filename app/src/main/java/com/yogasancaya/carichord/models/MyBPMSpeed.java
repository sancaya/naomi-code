package com.yogasancaya.carichord.models;

import androidx.annotation.Nullable;

public class MyBPMSpeed {
    public Integer secondSpeed = 18;
    public float maxBPM;
    public float minBPM;
    public String name;
    boolean isActive;

    public MyBPMSpeed(){

    }
    public MyBPMSpeed(String name,float maxBPM,float minBPM,@Nullable Integer secondSpeed){
        this.name = name;
        this.maxBPM = maxBPM;
        this.minBPM = minBPM;
        if(secondSpeed!=null){
            this.secondSpeed = secondSpeed;
        }
    }

    public boolean isActive() {
        return isActive;
    }
    public int GetInSecond(){
        return secondSpeed;
    }
}
