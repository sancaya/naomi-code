package com.yogasancaya.carichord.api;

import android.os.AsyncTask;
import android.util.Log;

import androidx.annotation.Nullable;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

public class RapidAPI extends AsyncTask<String, Void, String> {
    private static final String TAG = "TEST";
    RapidAPIListener apiListener;
    String reqUrl, xRapidapiKey,xRapidapiHost;
    public RapidAPI(String reqUrl, String xRapidapiKey, @Nullable String xRapidapiHost, RapidAPIListener apiListener){
        this.reqUrl = reqUrl;
        this.apiListener = apiListener;
        this.xRapidapiKey = xRapidapiKey;
        this.xRapidapiHost = xRapidapiHost;
    }
    String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line).append('\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }

    @Override
    protected String doInBackground(String... strings) {
        String response = "unstable";
        try {
            URL url = new URL(reqUrl);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("x-rapidapi-key", xRapidapiKey);
            conn.setRequestProperty("x-rapidapi-host", xRapidapiHost);
            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);

        } catch (MalformedURLException e) {
            apiListener.failed("MalformedURLException: " + e.getMessage());
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            apiListener.failed("ProtocolException: " + e.getMessage());
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            apiListener.failed("IOException: " + e.getMessage());
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            apiListener.failed("Exception: " + e.getMessage());
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return response;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        apiListener.sucess(s);
    }
}
