package com.yogasancaya.carichord.api;

import android.os.AsyncTask;
import android.text.Html;
import android.util.Log;

import androidx.annotation.NonNull;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class NyolongGambarGoogleAPI extends AsyncTask<String, Void, List<String>> {

    String TAG = "NyolongGambar";
    String cari = "https://www.google.com/search?q=";
    String addCari = "&tbm=isch&hl=id&tbs=isz:l&sa=X&biw=1903&bih=969";
    NyolongGoggleListener listener;

    String _url;
    public NyolongGambarGoogleAPI(@NonNull String query, @NonNull NyolongGoggleListener listener){
        _url = cari+query+addCari;
        this.listener = listener;
    }

String convertStreamToString(InputStream is) {
    BufferedReader reader = new BufferedReader(new InputStreamReader(is));
    StringBuilder sb = new StringBuilder();

    String line;
    try {
        while ((line = reader.readLine()) != null) {
            sb.append(line).append('\n');
        }
    } catch (IOException e) {
        e.printStackTrace();
    } finally {
        try {
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    return sb.toString();
}
    @Override
    protected List<String> doInBackground(String... strings) {
        String response = "unstable";
        List<String> raw = new ArrayList<>();
        try {
            URL url = new URL(_url);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            response = convertStreamToString(in);
            String se = response.toString();

            String[] u = se.split("\"");
            int y = 0;
            do{
                if(u[y].contains("http")&&u[y].contains(".jpg")
                        &&(u[y].contains("asset-a.grid.id")
                        ||u[y].contains("i.ytimg")
                        ||u[y].contains("dailysia")
                        ||u[y].contains("matalelaki")
                        ||u[y].contains("blogspot")
                        ||u[y].contains("wallpaper")
                )
                ){
                    raw.add(u[y].toString());
                }
                y++;
            } while (y<u.length);
            Log.d(TAG, ": "+raw+" ss "+u.length);
        } catch (MalformedURLException e) {
            Log.e(TAG, "MalformedURLException: " + e.getMessage());
        } catch (ProtocolException e) {
            Log.e(TAG, "ProtocolException: " + e.getMessage());
        } catch (IOException e) {
            Log.e(TAG, "IOException: " + e.getMessage());
        } catch (Exception e) {
            Log.e(TAG, "Exception: " + e.getMessage());
        }
        return raw;
    }

    @Override
    protected void onPostExecute(List<String> s) {
        super.onPostExecute(s);
        listener.Result(s);
    }
}
