package com.yogasancaya.carichord;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.yogasancaya.carichord.api.NyolongGambarGoogleAPI;
import com.yogasancaya.carichord.ui.home.HomeFragment;
import com.yogasancaya.carichord.ui.request.RequestFragment;
import com.yogasancaya.carichord.ui.tuner.TunerFragment;

import androidx.annotation.IdRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.NavOptions;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.security.acl.Permission;

public class BasePageActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener {
    public static final String PREFS_FILE = "prefs_file";
    public static String valQ = "";
    public static String valPage = "";
    public static String valPageArtis = "";

    private static final String USE_DARK_MODE = "use_dark_mode";

    private AppBarConfiguration mAppBarConfiguration;
    NavController navController;
    NavOptions navOptions;

    public static boolean isDarkModeEnabled;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setTheme();
        setContentView(R.layout.activity_base_page);


        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        BottomNavigationView navigation = findViewById(R.id.bottomNavigationView);
        navigation.setOnNavigationItemSelectedListener(this);

        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_tuner, R.id.nav_request)
                .setDrawerLayout(drawer)
                .build();
        navController = Navigation.findNavController(this, R.id.nav_host_fragment);
//        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
        NavigationUI.setupWithNavController(navigation,navController);

        navOptions = new NavOptions.Builder()
                .setLaunchSingleTop(true)  // Used to prevent multiple copies of the same destination
                .setEnterAnim(R.anim.fade_in)
                .setExitAnim(R.anim.fade_out)
                .setPopEnterAnim(R.anim.slide_in)
                .setPopExitAnim(R.anim.slide_out)
                .build();

    }
    public void SetFragmentHome(){
        navController.navigate(R.id.nav_home);
    }
    public void SetFragmentTuner(){
        navController.navigate(R.id.nav_tuner);
    }
    public void SetFragmentRequest(){
        navController.navigate(R.id.nav_request);
    }
    public void SetFragmentArtistAZ(){
        navController.navigate(R.id.nav_artiz_a_z);
    }
    public void SetFragmentArtistPage(String urlArtis){
        valPageArtis = urlArtis;
        navController.navigate(R.id.nav_artis_page);
    }


    public void SetFragment(@IdRes int id,@NonNull Activity activity,String arg){
        switch (id){
            case R.id.nav_search :
                valQ = arg;
                break;
            case R.id.nav_page :
                valPage = arg;
                break;
            default:
                break;
        }
        navController.navigate(id,null,navOptions);
    }

    private void setTheme() {
        final SharedPreferences preferences = getSharedPreferences(PREFS_FILE,
                MODE_PRIVATE);
        isDarkModeEnabled = preferences.getBoolean(USE_DARK_MODE, true);

        int mode;
        if (isDarkModeEnabled) {
            mode = AppCompatDelegate.MODE_NIGHT_YES;
        }else {
            mode = AppCompatDelegate.MODE_NIGHT_NO;
        }

        AppCompatDelegate.setDefaultNightMode(mode);
    }

    void SaveTheme(boolean a){
        final SharedPreferences preferences = getSharedPreferences(PREFS_FILE,
                MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(USE_DARK_MODE, a);
        editor.apply();
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    Menu menuItm;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        super.onCreateOptionsMenu(menu);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.base_page, menu);
        this.menuItm = menu;
        if (menu.findItem(R.id.sub_setting_item) != null) {
            final SharedPreferences preferences = getSharedPreferences(PREFS_FILE,
                    MODE_PRIVATE);
            isDarkModeEnabled = preferences.getBoolean(USE_DARK_MODE, false);
            if(isDarkModeEnabled){
                SpannableString s = new SpannableString("Light Mode");
                s.setSpan(new ForegroundColorSpan(Color.WHITE), 0, s.length(), 0);
                menu.findItem(R.id.sub_setting_item).setTitle(s);
            }else {

                menu.findItem(R.id.sub_setting_item).setTitle("Dark Mode");
            }

        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.action_settings:
                return true;
            case R.id.sub_setting_item:
                final SharedPreferences preferences = getSharedPreferences(PREFS_FILE,
                        MODE_PRIVATE);
                isDarkModeEnabled = preferences.getBoolean(USE_DARK_MODE, false);
                if(isDarkModeEnabled){
                    SaveTheme(false);
                }else {
                    SaveTheme(true);
                }
                setTheme();
                return true;
        }
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        navController.navigate(item.getItemId(),null,navOptions);
        return false;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void startActivityForResult(Intent intent, int requestCode, @Nullable Bundle options) {
        super.startActivityForResult(intent, requestCode, options);
    }
}