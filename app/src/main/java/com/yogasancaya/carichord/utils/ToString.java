package com.yogasancaya.carichord.utils;

import android.text.Html;
import android.text.TextUtils;
import android.util.Log;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class ToString {
    public String encodeHtml(String val){
        return Html.fromHtml(val).toString();
    }

    public String encodeHtmlChordRaw(String val){
        String la = new Tranposer().JustRemove(val);
        String gu = la.replace("&nbsp;", "  ");
        return gu.replace("<br />","\n");
    }
    public String encodeHtmlChordClean(String val){
        String cleancode = val.replace(Tranposer.Code," ");
        String cleanOther = cleancode.replace("<a class=\"tbi-tooltip\" href=\"#\">","");
        return cleanOther;
    }
    public String encodeDate(String val){

        try {
            SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = inputFormat.parse(val);
            Date now = new Date();
            long seconds=TimeUnit.MILLISECONDS.toSeconds(now.getTime() - date.getTime());
            long minutes=TimeUnit.MILLISECONDS.toMinutes(now.getTime() - date.getTime());
            long hours=TimeUnit.MILLISECONDS.toHours(now.getTime() - date.getTime());
            long days= TimeUnit.MILLISECONDS.toDays(now.getTime() - date.getTime());

            if(seconds<60)
            {
                return seconds+" seconds ago";
            }
            else if(minutes<60)
            {
                return minutes+" minutes ago";
            }
            else if(hours<24)
            {
                return hours+" hours ago";
            }
            else
            {
                return days+" days ago";
            }
        } catch (ParseException e) {
            e.printStackTrace();
            return "";

        }
    }

}
