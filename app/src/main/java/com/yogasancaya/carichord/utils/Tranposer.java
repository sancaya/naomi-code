package com.yogasancaya.carichord.utils;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.Typeface;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.text.style.StyleSpan;
import android.util.Log;
import android.widget.TextView;

import androidx.annotation.ColorInt;
import androidx.annotation.ColorRes;
import androidx.core.widget.ContentLoadingProgressBar;

import com.yogasancaya.carichord.BasePageActivity;
import com.yogasancaya.carichord.R;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class Tranposer {

    String add = "[";
    public static String Code = "$%";
    public static String Codew = "%$";


    String ab1 = "]";

    List<String> dataChordS;
    List<String> dataChordSm;

    public Tranposer(){
        dataChordS = new ArrayList<>();
    }
    public String JustRemove(String dataChord){
        String new1 = dataChord;
        String new2 = new1.replace(add,Code);

        String new3 = new2.replace(ab1,Code);

        return new3;
    }
    public List<TextView> BeSpan(String rawChordSave, Activity activity,float sizeFont){
        SpannableString spannableString = new SpannableString(rawChordSave);

        String a = Html.toHtml(spannableString);
        String b = a.replace("<p dir=\"ltr\">","<br>");
        String[] u = b.split("<br>");
        List<TextView> hyu = new ArrayList<>();
        for(int t = 0;t<u.length;t++){
            TextView tv = new TextView(activity);

            SpannableString ss = new SpannableString(rmnbsp(u[t]));
            StyleSpan boldSpan = new StyleSpan(Typeface.BOLD);
            ss.setSpan(boldSpan, 0, ss.length(),Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
            tv.setText(ss);
            tv.setTextSize(sizeFont);
            tv.setPadding(10,0,10,0);

            if(u[t].toLowerCase().contains(Code.toLowerCase())){
                if(BasePageActivity.isDarkModeEnabled) tv.setTextColor(Color.parseColor("#fcb212"));
                else tv.setTextColor(Color.parseColor("#FFFFFFFF"));
            }

            hyu.add(tv);
        }
        return hyu;
    }
    String rmnbsp(String new1){
        String new2 = new1.replace("&nbsp;"," ");
        String new3 = new2.replace(Code,"");
        String new4 = new3.replace("</p>","");
        String new5 = new4.replace("&#13;","");
        String new6 = new5.replace("&#8230;","");
        return JustRemove(new6);
    }
    public String Up(String rawChordSave){
        String rawChordSave1 = rawChordSave.replace(Code+"A"+ Code,Code+"A#"+Codew+Code);
        String rawChordSave2 = rawChordSave1.replace(Code+"A#"+ Code,Code+"B"+Codew+Code);
        String rawChordSave3 = rawChordSave2.replace(Code+"Bb"+ Code,Code+"B"+Codew+Code);
        String rawChordSave4 = rawChordSave3.replace(Code+"B"+ Code,Code+"C"+Codew+Code);
        String rawChordSave5 = rawChordSave4.replace(Code+"C"+ Code,Code+"C#"+Codew+Code);
        String rawChordSave6 = rawChordSave5.replace(Code+"C#"+ Code,Code+"D"+Codew+Code);
        String rawChordSave7 = rawChordSave6.replace(Code+"Db"+ Code,Code+"D"+Codew+Code);
        String rawChordSave8 = rawChordSave7.replace(Code+"D"+ Code,Code+"D#"+Codew+Code);
        String rawChordSave9 = rawChordSave8.replace(Code+"D#"+ Code,Code+"E"+Codew+Code);
        String rawChordSave10 = rawChordSave9.replace(Code+"Eb"+ Code,Code+"E"+Codew+Code);
        String rawChordSave11 = rawChordSave10.replace(Code+"E"+ Code,Code+"F"+Codew+Code);
        String rawChordSave12 = rawChordSave11.replace(Code+"F"+ Code,Code+"F#"+Codew+Code);
        String rawChordSave13 = rawChordSave12.replace(Code+"F#"+ Code,Code+"G"+Codew+Code);
        String rawChordSave14 = rawChordSave13.replace(Code+"Gb"+ Code,Code+"G"+Codew+Code);
        String rawChordSave15 = rawChordSave14.replace(Code+"G"+ Code,Code+"G#"+Codew+Code);
        String rawChordSave16 = rawChordSave15.replace(Code+"G#"+ Code,Code+"A"+Codew+Code);
        String rawChordSave17 = rawChordSave16.replace(Code+"Ab"+ Code,Code+"A"+Codew+Code);

        String rawChordSavem1 = rawChordSave17.replace(Code+"Am"+ Code,Code+"A#m"+Codew+Code);
        String rawChordSavem2 = rawChordSavem1.replace(Code+"A#m"+ Code,Code+"Bm"+Codew+Code);
        String rawChordSavem3 = rawChordSavem2.replace(Code+"Bbm"+ Code,Code+"Bm"+Codew+Code);
        String rawChordSavem4 = rawChordSavem3.replace(Code+"Bm"+ Code,Code+"Cm"+Codew+Code);
        String rawChordSavem5 = rawChordSavem4.replace(Code+"Cm"+ Code,Code+"C#m"+Codew+Code);
        String rawChordSavem6 = rawChordSavem5.replace(Code+"C#m"+ Code,Code+"Dm"+Codew+Code);
        String rawChordSavem7 = rawChordSavem6.replace(Code+"Dbm"+ Code,Code+"Dm"+Codew+Code);
        String rawChordSavem8 = rawChordSavem7.replace(Code+"Dm"+ Code,Code+"D#m"+Codew+Code);
        String rawChordSavem9 = rawChordSavem8.replace(Code+"D#m"+ Code,Code+"Em"+Codew+Code);
        String rawChordSavem10 = rawChordSavem9.replace(Code+"Ebm"+ Code,Code+"Em"+Codew+Code);
        String rawChordSavem11 = rawChordSavem10.replace(Code+"Em"+ Code,Code+"Fm"+Codew+Code);
        String rawChordSavem12 = rawChordSavem11.replace(Code+"Fm"+ Code,Code+"F#m"+Codew+Code);
        String rawChordSavem13 = rawChordSavem12.replace(Code+"F#m"+ Code,Code+"Gm"+Codew+Code);
        String rawChordSavem14 = rawChordSavem13.replace(Code+"Gbm"+ Code,Code+"Gm"+Codew+Code);
        String rawChordSavem15 = rawChordSavem14.replace(Code+"Gm"+ Code,Code+"G#m"+Codew+Code);
        String rawChordSavem16 = rawChordSavem15.replace(Code+"G#m"+ Code,Code+"Am"+Codew+Code);
        String rawChordSavem17 = rawChordSavem16.replace(Code+"Abm"+ Code,Code+"Am"+Codew+Code);
        return rawChordSavem17.replace(Codew,"");
    }

    public String Down(String rawChordSave){
        String rawChordSave1 = rawChordSave.replace(Code+"A"+ Code,Code+"G#"+Codew+Code);
        String rawChordSave2 = rawChordSave1.replace(Code+"A#"+ Code,Code+"A"+Codew+Code);
        String rawChordSave3 = rawChordSave2.replace(Code+"Bb"+ Code,Code+"A"+Codew+Code);
        String rawChordSave4 = rawChordSave3.replace(Code+"B"+ Code,Code+"A#"+Codew+Code);
        String rawChordSave5 = rawChordSave4.replace(Code+"C"+ Code,Code+"B"+Codew+Code);
        String rawChordSave6 = rawChordSave5.replace(Code+"C#"+ Code,Code+"C"+Codew+Code);
        String rawChordSave7 = rawChordSave6.replace(Code+"Db"+ Code,Code+"C"+Codew+Code);
        String rawChordSave8 = rawChordSave7.replace(Code+"D"+ Code,Code+"C#"+Codew+Code);
        String rawChordSave9 = rawChordSave8.replace(Code+"D#"+ Code,Code+"D"+Codew+Code);
        String rawChordSave10 = rawChordSave9.replace(Code+"Eb"+ Code,Code+"D"+Codew+Code);
        String rawChordSave11 = rawChordSave10.replace(Code+"E"+ Code,Code+"D#"+Codew+Code);
        String rawChordSave12 = rawChordSave11.replace(Code+"F"+ Code,Code+"E"+Codew+Code);
        String rawChordSave13 = rawChordSave12.replace(Code+"F#"+ Code,Code+"F"+Codew+Code);
        String rawChordSave14 = rawChordSave13.replace(Code+"Gb"+ Code,Code+"F#"+Codew+Code);
        String rawChordSave15 = rawChordSave14.replace(Code+"G"+ Code,Code+"F#"+Codew+Code);
        String rawChordSave16 = rawChordSave15.replace(Code+"G#"+ Code,Code+"G"+Codew+Code);
        String rawChordSave17 = rawChordSave16.replace(Code+"Ab"+ Code,Code+"G"+Codew+Code);

        String rawChordSavem1 = rawChordSave17.replace(Code+"Am"+ Code,Code+"G#m"+Codew+Code);
        String rawChordSavem2 = rawChordSavem1.replace(Code+"A#m"+ Code,Code+"Am"+Codew+Code);
        String rawChordSavem3 = rawChordSavem2.replace(Code+"Bbm"+ Code,Code+"Am"+Codew+Code);
        String rawChordSavem4 = rawChordSavem3.replace(Code+"Bm"+ Code,Code+"A#m"+Codew+Code);
        String rawChordSavem5 = rawChordSavem4.replace(Code+"Cm"+ Code,Code+"Bm"+Codew+Code);
        String rawChordSavem6 = rawChordSavem5.replace(Code+"C#m"+ Code,Code+"Cm"+Codew+Code);
        String rawChordSavem7 = rawChordSavem6.replace(Code+"Dbm"+ Code,Code+"C#m"+Codew+Code);
        String rawChordSavem8 = rawChordSavem7.replace(Code+"Dm"+ Code,Code+"C#m"+Codew+Code);
        String rawChordSavem9 = rawChordSavem8.replace(Code+"D#m"+ Code,Code+"Dm"+Codew+Code);
        String rawChordSavem10 = rawChordSavem9.replace(Code+"Ebm"+ Code,Code+"Dm"+Codew+Code);
        String rawChordSavem11 = rawChordSavem10.replace(Code+"Em"+ Code,Code+"D#m"+Codew+Code);
        String rawChordSavem12 = rawChordSavem11.replace(Code+"Fm"+ Code,Code+"Em"+Codew+Code);
        String rawChordSavem13 = rawChordSavem12.replace(Code+"F#m"+ Code,Code+"Fm"+Codew+Code);
        String rawChordSavem14 = rawChordSavem13.replace(Code+"Gbm"+ Code,Code+"F#m"+Codew+Code);
        String rawChordSavem15 = rawChordSavem14.replace(Code+"Gm"+ Code,Code+"F#m"+Codew+Code);
        String rawChordSavem16 = rawChordSavem15.replace(Code+"G#m"+ Code,Code+"Gm"+Codew+Code);
        String rawChordSavem17 = rawChordSavem16.replace(Code+"Abm"+ Code,Code+"Gm"+Codew+Code);
        return rawChordSavem17.replace(Codew,"");
    }
}
