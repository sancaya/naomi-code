package com.yogasancaya.carichord.utils;

import com.yogasancaya.carichord.models.PitchDifference;
import com.yogasancaya.carichord.tuning.Note;
import com.yogasancaya.carichord.tuning.Tuning;
import com.yogasancaya.carichord.tuning.instrument.NoteFrequencyCalculator;
import com.yogasancaya.carichord.ui.tuner.TunerFragment;

import java.util.Arrays;

public class PitchComparator {
    public static PitchDifference retrieveNote(float pitch) {
        Tuning tuning = TunerFragment.getCurrentTuning();
        int referencePitch = TunerFragment.getReferencePitch();

        Note[] tuningNotes = tuning.getNotes();
        Note[] notes;

        if (TunerFragment.isAutoModeEnabled()) {
            notes = tuningNotes;
        } else {
            notes = new Note[]{tuningNotes[TunerFragment.getReferencePosition()]};
        }

        NoteFrequencyCalculator noteFrequencyCalculator =
                new NoteFrequencyCalculator(referencePitch);

        Arrays.sort(notes, (o1, o2) ->
                Double.compare(noteFrequencyCalculator.getFrequency(o1),
                        noteFrequencyCalculator.getFrequency(o2)));

        double minCentDifference = Float.POSITIVE_INFINITY;
        Note closest = notes[0];
        for (Note note : notes) {
            double frequency = noteFrequencyCalculator.getFrequency(note);
            double centDifference = 1200d * log2(pitch / frequency);

            if (Math.abs(centDifference) < Math.abs(minCentDifference)) {
                minCentDifference = centDifference;
                closest = note;
            }
        }

        return new PitchDifference(closest, minCentDifference);
    }

    private static double log2(double number) {
        return Math.log(number) / Math.log(2);
    }
}
