package com.yogasancaya.carichord.ui.artist;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.yogasancaya.carichord.R;
import com.yogasancaya.carichord.models.DataArtist;
import com.yogasancaya.carichord.models.DataSearch;
import com.yogasancaya.carichord.ui.search.HasilSearchAdapter;
import com.yogasancaya.carichord.ui.search.SearchClick;
import com.yogasancaya.carichord.utils.ToString;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class ArtistAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    DataArtist dataSearch;
    ArtisItemClick searchClick;

    public ArtistAdapter(DataArtist dataSearch) {
        this.dataSearch = dataSearch;
    }

    public ArtistAdapter(DataArtist dataSearch, ArtisItemClick searchClick) {
        this.dataSearch = dataSearch;
        this.searchClick = searchClick;
    }

    public void AddData(DataArtist dataSearch) {
        this.dataSearch.results.addAll(dataSearch.results);
    }

    @Override
    public int getItemViewType(int position) {
        return dataSearch.results.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_artist_simple, parent, false);
            return new ArtistAdapter.ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lead_more, parent, false);
            return new ArtistAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ArtistAdapter.ItemViewHolder) {
            populateItemRows((ArtistAdapter.ItemViewHolder) holder, position);
        } else if (holder instanceof ArtistAdapter.LoadingViewHolder) {
            showLoadingView((ArtistAdapter.LoadingViewHolder) holder, position);
        }
    }

    String ytImage(String ytId) {
        return "https://img.youtube.com/vi/" + ytId + "/default.jpg";
    }

    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public int getItemCount() {
        return dataSearch.results == null ? 0 : dataSearch.results.size();
    }

    void populateItemRows(ArtistAdapter.ItemViewHolder holder, int position) {
        holder.name.setText(new ToString().encodeHtml(dataSearch.results.get(position).getArtis()));
        holder.chord.setText(new ToString().encodeHtml(dataSearch.results.get(position).getJumlahChord()));
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchClick != null)
                    searchClick.OnClick(position,dataSearch.results.get(position));
            }
        });
    }

    void showLoadingView(ArtistAdapter.LoadingViewHolder holder, int position) {
        //TODO Loading Adapter Search
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView name;
        public TextView chord;
        public View item;

        public ItemViewHolder(View view) {
            super(view);
            name = (TextView) view.findViewById(R.id.simple_artist_name);
            chord = (TextView) view.findViewById(R.id.simple_artist_chord);
            // Define click listener for the ViewHolder's View
            item = view;
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }
}