package com.yogasancaya.carichord.ui.artist;

import com.yogasancaya.carichord.models.DataArtisResult;

public interface ArtisItemClick {
    void OnClick(int pos, DataArtisResult searchResult);
}
