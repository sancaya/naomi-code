package com.yogasancaya.carichord.ui.tuner;

import static android.content.Context.MODE_PRIVATE;

import static com.yogasancaya.carichord.BasePageActivity.PREFS_FILE;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.NumberPicker;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;

import com.ekn.gruzer.gaugelibrary.HalfGauge;
import com.yogasancaya.carichord.BasePageActivity;
import com.yogasancaya.carichord.R;
import com.yogasancaya.carichord.models.PitchDifference;
import com.yogasancaya.carichord.tuning.TaskCallbacks;
import com.yogasancaya.carichord.tuning.TunerView;
import com.yogasancaya.carichord.tuning.Tuning;
import com.yogasancaya.carichord.utils.PitchComparator;
import com.yogasancaya.carichord.utils.Sampler;
import com.yogasancaya.carichord.utils.TuningMapper;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.io.android.AudioDispatcherFactory;
import be.tarsos.dsp.pitch.PitchDetectionHandler;
import be.tarsos.dsp.pitch.PitchProcessor;
import be.tarsos.dsp.pitch.PitchProcessor.PitchEstimationAlgorithm;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TunerFragment extends Fragment implements TaskCallbacks, NumberPicker.OnValueChangeListener {

    public TunerViewModel tunerViewModel;

    public static final int RECORD_AUDIO_PERMISSION = 0;

    public static final String USE_SCIENTIFIC_NOTATION = "use_scientific_notation";
    public static final String CURRENT_TUNING = "current_tuning";
    public static final String REFERENCE_PITCH = "reference_pitch";


    private static int tuningPosition = 0;

    private static int referencePitch;
    private static int referencePosition;
    private static boolean isAutoModeEnabled = true;

    private static final int SAMPLE_RATE = 44100;
    private static final int BUFFER_SIZE = 1024 * 4;
    private static final int OVERLAP = 768 * 4;
    private static final int MIN_ITEMS_COUNT = 15;
    public static boolean IS_RECORDING;
    private static List<PitchDifference> pitchDifferences = new ArrayList<>();
    private static TaskCallbacks taskCallbacks;
    private PitchListener pitchListener;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        taskCallbacks = this;
    }
    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            taskCallbacks = this;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        ///RetainInstance(true);


    }

    @Override
    public void onDetach() {
        super.onDetach();

        taskCallbacks = null;
        if(pitchListener == null)return;
        pitchListener.cancel(true);
    }

    @Override
    public void onPause() {
        super.onPause();
        if(pitchListener == null)return;
        pitchListener.cancel(true);
    }

    @Override
    public void onResume() {
        super.onResume();

        if(pitchListener == null)return;
        if (pitchListener.isCancelled()) {
            pitchListener = new PitchListener();
            pitchListener.execute();
        }
    }



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        tunerViewModel = new ViewModelProvider(this).get(TunerViewModel.class);
        View root = inflater.inflate(R.layout.fragment_tuner, container, false);
        Init(root);
        return root;
    }

    public static Tuning getCurrentTuning() {
        return TuningMapper.getTuningFromPosition(tuningPosition);
    }
    public static boolean isAutoModeEnabled() {
        return isAutoModeEnabled;
    }
    public static int getReferencePitch() {
        return referencePitch;
    }
    public static int getReferencePosition() {
        return referencePosition - 1;
    }
    HalfGauge indicator;
    TextView indicatorNote,note_sign;
    void Init(View view){
        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.RECORD_AUDIO);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            requestAudioPermission();
        } else {
            startTuning();
        }



        indicator = view.findViewById(R.id.indicator);
        indicator.setMaxValue(50);
        indicator.setMinValue(-50);
        indicator.setValue(0);
        indicator.enableAnimation(true);
        indicator.setEnableBackGroundShadow(true);
        indicator.setValueColor(R.color.primary);

        indicatorNote = view.findViewById(R.id.note_indicator);
        note_sign = view.findViewById(R.id.note_sign);
//        indicatorNote.setVisibility(View.GONE);
//        note_sign.setVisibility(View.GONE);
    }


    @Override
    public void onValueChange(NumberPicker picker, int oldVal, int newVal) {
        String tag = String.valueOf(picker.getTag());
        if ("reference_pitch_picker".equalsIgnoreCase(tag)) {
            final SharedPreferences preferences = getActivity().getSharedPreferences(PREFS_FILE,
                    MODE_PRIVATE);

            SharedPreferences.Editor editor = preferences.edit();
            editor.putInt(REFERENCE_PITCH, newVal);
            editor.apply();

            setReferencePitch();
        } else if ("note_picker".equalsIgnoreCase(tag)) {
            isAutoModeEnabled = newVal == 0;

            referencePosition = newVal;
        }
    }

String TAG = "TunerFragment";
    @SuppressLint("SetTextI18n")
    @Override
    public void onProgressUpdate(PitchDifference percent) {

        if(percent==null){
//            tunerView.setPitchDifference(percent);
//            tunerView.invalidate();
        }else {
            Log.d(TAG, String.valueOf(percent.closest)+" "+String.valueOf(percent.deviation));

            Double gu = percent.deviation;
            indicator.setValue(gu.intValue());
            note_sign.setText(String.valueOf(percent.closest.getOctave()));
            indicatorNote.setText(percent.closest.getName().getScientific()+percent.closest.getSign());
            indicatorNote.setVisibility(View.VISIBLE);
            note_sign.setVisibility(View.VISIBLE);


        }
    }


    private static class PitchListener extends AsyncTask<Void, PitchDifference, Void> {

        private AudioDispatcher audioDispatcher;

        @Override
        protected Void doInBackground(Void... params) {
            PitchDetectionHandler pitchDetectionHandler = (pitchDetectionResult, audioEvent) -> {

                if (isCancelled()) {
                    stopAudioDispatcher();
                    return;
                }

                if (!IS_RECORDING) {
                    IS_RECORDING = true;
                    publishProgress();
                }

                float pitch = pitchDetectionResult.getPitch();

                if (pitch != -1) {
                    PitchDifference pitchDifference = PitchComparator.retrieveNote(pitch);

                    pitchDifferences.add(pitchDifference);

                    if (pitchDifferences.size() >= MIN_ITEMS_COUNT) {
                        PitchDifference average =
                                Sampler.calculateAverageDifference(pitchDifferences);

                        publishProgress(average);

                        pitchDifferences.clear();
                    }
                }
            };

            PitchProcessor pitchProcessor = new PitchProcessor(PitchEstimationAlgorithm.FFT_YIN,
                    SAMPLE_RATE,
                    BUFFER_SIZE, pitchDetectionHandler);

            audioDispatcher = AudioDispatcherFactory.fromDefaultMicrophone(SAMPLE_RATE,
                    BUFFER_SIZE, OVERLAP);

            audioDispatcher.addAudioProcessor(pitchProcessor);

            audioDispatcher.run();

            return null;
        }

        @Override
        protected void onCancelled(Void result) {
            stopAudioDispatcher();
        }

        @Override
        protected void onProgressUpdate(PitchDifference... pitchDifference) {
            if (taskCallbacks != null) {
                if (pitchDifference.length > 0) {
                    taskCallbacks.onProgressUpdate(pitchDifference[0]);
                } else {
                    taskCallbacks.onProgressUpdate(null);
                }
            }
        }

        private void stopAudioDispatcher() {
            if (audioDispatcher != null && !audioDispatcher.isStopped()) {
                audioDispatcher.stop();
                IS_RECORDING = false;
            }
        }
    }

    private void startTuning() {
        setTuning();
        setReferencePitch();

        getActivity().getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        pitchListener = new PitchListener();
        pitchListener.execute();
    }

    private void setTuning() {
        final SharedPreferences preferences = getActivity().getSharedPreferences(PREFS_FILE,
                MODE_PRIVATE);
        tuningPosition = preferences.getInt(CURRENT_TUNING, 0);

//        int textColorDark = getResources().getColor(R.color.colorTextDark);

//        MaterialSpinner spinner = findViewById(R.id.tuning);
//        MaterialSpinnerAdapter<String> adapter = new MaterialSpinnerAdapter<>(this,
//                Arrays.asList(getResources().getStringArray(R.array.tunings)));
        List<String> tuning = Arrays.asList(getResources().getStringArray(R.array.tunings));
    }

    private void setReferencePitch() {
        final SharedPreferences preferences = getActivity().getSharedPreferences(PREFS_FILE,
                MODE_PRIVATE);
        referencePitch = preferences.getInt(REFERENCE_PITCH, 440);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == RECORD_AUDIO_PERMISSION) {
            if (grantResults.length > 0) {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startTuning();
                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle(R.string.permission_grant_access_title);
                    alertDialog.setMessage(getString(R.string.permission_grant_access_description));
                    alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.permission_allow_button),
                            (dialog, which) -> {
                                dialog.dismiss();
                                goToSettings();
                                ((BasePageActivity)getActivity()).SetFragmentHome();
                            });
                    alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE,getString(R.string.permission_exit_button),
                            (dialog, which) -> {
                                dialog.dismiss();
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                    ((BasePageActivity)getActivity()).SetFragmentHome();
                                } else {
                                    ((BasePageActivity)getActivity()).SetFragmentHome();
                                }
                            });
                    alertDialog.show();
                }
            }
        }
    }

    private void goToSettings() {
        Intent myAppSettings = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS, Uri.parse("package:" + getActivity().getPackageName()));
        myAppSettings.addCategory(Intent.CATEGORY_DEFAULT);
        myAppSettings.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getActivity().startActivityForResult(myAppSettings, 15);
    }



    private void requestAudioPermission() {
        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
        alertDialog.setTitle(R.string.permission_title);
        alertDialog.setMessage(getString(R.string.permission_description));
        alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, getString(R.string.permission_allow_button),
                (dialog, which) -> {
                    dialog.dismiss();
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.RECORD_AUDIO},
                            RECORD_AUDIO_PERMISSION);
                });
        alertDialog.show();

    }
}