package com.yogasancaya.carichord.ui.artistpage;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yogasancaya.carichord.R;
import com.yogasancaya.carichord.api.NyolongGambarGoogleAPI;
import com.yogasancaya.carichord.api.NyolongGoggleListener;
import com.yogasancaya.carichord.models.DataArtist;
import com.yogasancaya.carichord.models.DataSearch;
import com.yogasancaya.carichord.models.SingleArtisData;
import com.yogasancaya.carichord.ui.search.HasilSearchAdapter;
import com.yogasancaya.carichord.ui.search.SearchClick;
import com.yogasancaya.carichord.utils.ToString;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

public class ArtisPageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    SingleArtisData dataSearch;
    ArtisAlbumClick searchClick;
    Activity activity;
    List<String> urlGambar;
    public ArtisPageAdapter(SingleArtisData dataSearch){
        this.dataSearch = dataSearch;
    }
    public ArtisPageAdapter(SingleArtisData dataSearch, ArtisAlbumClick searchClick, Activity activity,List<String> urlGambar){
        this.dataSearch = dataSearch;
        this.searchClick = searchClick;
        this.activity = activity;
        this.urlGambar = urlGambar;
    }
    public int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }
    public void AddData(SingleArtisData dataSearch){
        this.dataSearch.results.addAll(dataSearch.results);
    }

    @Override
    public int getItemViewType(int position) {
        return dataSearch.results.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_artis_album_terbaru, parent, false);
            return new ArtisPageAdapter.ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lead_more, parent, false);
            return new ArtisPageAdapter.LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ArtisPageAdapter.ItemViewHolder) {
            populateItemRows((ArtisPageAdapter.ItemViewHolder) holder, position);
        } else if (holder instanceof ArtisPageAdapter.LoadingViewHolder) {
            showLoadingView((ArtisPageAdapter.LoadingViewHolder) holder, position);
        }
    }
    String ytImage(String ytId){
        return "https://img.youtube.com/vi/"+ytId+"/default.jpg";
    }
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    @Override
    public int getItemCount() {
        return dataSearch.results == null? 0 :dataSearch.results.size();
    }

    void populateItemRows(ArtisPageAdapter.ItemViewHolder holder, int position){
        holder.title.setText(new ToString().encodeHtml(dataSearch.results.get(position).judul));
        holder.date.setText(new ToString().encodeDate(dataSearch.results.get(position).date));
//        holder.views.setText(dataSearch.results.get(position));

        Picasso.with(activity).load(urlGambar.get(getRandomNumber(0,urlGambar.size()))).fit().into(holder.thumbnail);
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(searchClick!=null) searchClick.OnClick(holder.getAdapterPosition(),dataSearch.results.get(position));
            }
        });
    }
    void showLoadingView(ArtisPageAdapter.LoadingViewHolder holder, int position){
        //TODO Loading Adapter Search
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView date;
        public TextView views;
        public View item;
        public ImageView thumbnail;
        public ItemViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            date = (TextView) view.findViewById(R.id.date);
            views = (TextView) view.findViewById(R.id.view);
            thumbnail = (ImageView) view.findViewById(R.id.image_thumbnail);
            // Define click listener for the ViewHolder's View
            item = view;
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }



}