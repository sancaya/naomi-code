package com.yogasancaya.carichord.ui.artistpage;

import com.yogasancaya.carichord.models.SingleArtisDataResult;

public interface ArtisAlbumClick {
    void OnClick(int pos, SingleArtisDataResult searchResult);
}
