package com.yogasancaya.carichord.ui.home;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.core.widget.NestedScrollView;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yogasancaya.carichord.BasePageActivity;
import com.yogasancaya.carichord.R;
import com.yogasancaya.carichord.api.API;
import com.yogasancaya.carichord.api.APIListener;
import com.yogasancaya.carichord.config.Info;
import com.yogasancaya.carichord.models.DataSearch;
import com.yogasancaya.carichord.models.DataSearchResult;
import com.yogasancaya.carichord.ui.search.HasilSearchAdapter;
import com.yogasancaya.carichord.ui.search.SearchClick;

import java.util.ArrayList;

public class HomeFragment extends Fragment {

    private HomeViewModel homeViewModel;

    EditText textCari;
    ImageButton cariBtn,caribtnMenu;

    ViewGroup _container;

    DataSearch dataSearch;
    HasilSearchAdapter searchAdapter;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        _container = container;
        homeViewModel =
                new ViewModelProvider(this).get(HomeViewModel.class);
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        //final TextView textView = root.findViewById(R.id.text_home);
        init(root);
        homeViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
               // textView.setText(s);
            }
        });
        return root;
    }
    NestedScrollView nestedScrollView;
    void init(View view){
        textCari = view.findViewById(R.id.cari_header_text);
        cariBtn = view.findViewById(R.id.cari_header);
        caribtnMenu = view.findViewById(R.id.cari_btn);
        nestedScrollView = view.findViewById(R.id.home_scroller);

        cariBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textCari.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(), getString(R.string.toast_input_not_field), Toast.LENGTH_SHORT).show();
                    textCari.requestFocus();
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }else {
                    ((BasePageActivity)getActivity()).SetFragment(R.id.nav_search,getActivity(),textCari.getText().toString());
                    ((BasePageActivity)getActivity()).hideKeyboard(getActivity());
                }
            }
        });
        caribtnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textCari.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(), getString(R.string.toast_input_not_field), Toast.LENGTH_SHORT).show();
                    textCari.requestFocus();
                    nestedScrollView.scrollTo(0,0);
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Activity.INPUT_METHOD_SERVICE);
                    imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                }else {
                    ((BasePageActivity)getActivity()).SetFragment(R.id.nav_search,getActivity(),textCari.getText().toString());
                    ((BasePageActivity)getActivity()).hideKeyboard(getActivity());
                }
            }
        });

        //Tuner
        ImageButton lLTuner = view.findViewById(R.id.home_tunner);
        lLTuner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BasePageActivity)getActivity()).SetFragmentTuner();
            }
        });
        //Request
        ImageButton lLRequest = view.findViewById(R.id.home_request);
        lLRequest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BasePageActivity)getActivity()).SetFragmentRequest();
            }
        });
        //Artis
        ImageButton lLRequestaz = view.findViewById(R.id.home_artist_az);
        lLRequestaz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((BasePageActivity)getActivity()).SetFragmentArtistAZ();
            }
        });

        InitPopular(view);
    }

    void InitPopular(View view){
        RecyclerView hasilCari = view.findViewById(R.id.new_rv);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
//        RecyclerView.LayoutManager manager = new GridLayoutManager(getActivity(),2);
        hasilCari.setLayoutManager(manager);
        dataSearch = new DataSearch();
        dataSearch.results = new ArrayList<>();
        dataSearch.results.add(null);
        searchAdapter = new HasilSearchAdapter(dataSearch, new SearchClick() {
            @Override
            public void OnClick(int a,DataSearchResult searchResult) {
                ((BasePageActivity) getActivity()).SetFragment(R.id.nav_page, getActivity(), searchResult.lagu);
            }
        });
        hasilCari.setAdapter(searchAdapter);
        searchAdapter.notifyDataSetChanged();
        new API(Info.URL + Info.QSearch("Happy"), new APIListener() {
            @Override
            public void failed(String err) {
                //tv_notice.setText("Error "+err);
            }

            @Override
            public void sucess(String v) {
                dataSearch.results.remove(dataSearch.results.size() - 1);
                int scrollPosition = dataSearch.results.size();
                searchAdapter.notifyItemRemoved(scrollPosition);
                dataSearch = new DataSearch(v);
                RecyclerView.LayoutManager manager = new GridLayoutManager(getActivity(),2);
                hasilCari.setLayoutManager(manager);
                if(dataSearch.total == 0){
                    return;
                }else {
                    searchAdapter = new HasilSearchAdapter(dataSearch, new SearchClick() {
                        @Override
                        public void OnClick(int a,DataSearchResult searchResult) {
                            ((BasePageActivity) getActivity()).SetFragment(R.id.nav_page, getActivity(), searchResult.lagu);
                        }
                    });
                    hasilCari.setAdapter(searchAdapter);
                    searchAdapter.notifyDataSetChanged();
                }
            }
        }).execute();


    }

    String getQuery(){
        if(BasePageActivity.valQ.isEmpty()){
            return "";
        }else {
            return BasePageActivity.valQ;
        }
    }
}