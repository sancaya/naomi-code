package com.yogasancaya.carichord.ui.artist;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.adapter.FragmentStateAdapter;
import androidx.viewpager2.widget.ViewPager2;

import java.util.List;

public class ArtistPagerAdapter extends FragmentStateAdapter {
    List<String> key;
    ViewPager2 viewPager;
    Lifecycle lifecycle;
    public ArtistPagerAdapter (FragmentActivity fa, List<String> key, Lifecycle lifecycle) {
        super(fa);
        this.key = key;
        this.lifecycle = lifecycle;
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        if(position==0){
            ArtistPagerFragment homeFragment = ArtistPagerFragment.newInstance(position,null);
            return homeFragment;
        }else {
            ArtistPagerFragment sportFragment = ArtistPagerFragment.newInstance(position,key.get(position));
            return sportFragment;
        }
    }

    @Override
    public int getItemCount() {
        return key.size();
    }
}
