package com.yogasancaya.carichord.ui.page;

import static android.content.Context.MODE_PRIVATE;
import static com.yogasancaya.carichord.BasePageActivity.PREFS_FILE;

import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.LinearInterpolator;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.google.android.youtube.player.YouTubeInitializationResult;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.PlayerConstants;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.YouTubePlayer;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.AbstractYouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.listeners.YouTubePlayerListener;
import com.pierfrancescosoffritti.androidyoutubeplayer.core.player.views.YouTubePlayerView;
import com.yogasancaya.carichord.BasePageActivity;
import com.yogasancaya.carichord.R;
import com.yogasancaya.carichord.api.API;
import com.yogasancaya.carichord.api.APIListener;
import com.yogasancaya.carichord.api.RapidAPI;
import com.yogasancaya.carichord.api.RapidAPIListener;
import com.yogasancaya.carichord.api.datas.RapidAPIDdata;
import com.yogasancaya.carichord.config.Info;
import com.yogasancaya.carichord.models.DataSearch;
import com.yogasancaya.carichord.models.DataSearchResult;
import com.yogasancaya.carichord.models.Lagu;
import com.yogasancaya.carichord.models.MyBPMSpeed;
import com.yogasancaya.carichord.models.YouTubeVideos;
import com.yogasancaya.carichord.models.audiostream.Data;
import com.yogasancaya.carichord.ui.search.HasilSearchAdapter;
import com.yogasancaya.carichord.ui.search.SearchClick;
import com.yogasancaya.carichord.ui.tuner.TunerFragment;
import com.yogasancaya.carichord.utils.ToString;
import com.yogasancaya.carichord.utils.Tranposer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class PageFragment extends Fragment implements View.OnScrollChangeListener, View.OnTouchListener {
    String TAG = "Test";
    public static final String REFERENCE_SIZE_FONT = "reference_sizepont";
    public static final String REFERENCE_SPEED = "reference_speed";

    TextView tv_judul,tv_body,tvbtn_scroll;
    static String rawChordSave;
    YouTubeVideos youTubeVideos;
    YouTubePlayerView tubePlayer;

    ImageButton tUp,tDown,fontUp,fontDown;
    View tPlayMp3;
    WebView ytVid;

    MediaPlayer mediaplayer;

    LinearLayout linearLayout,btn_scroll,btn_seting,view_speed;
    ScrollView scrollView;
    float sizeFontP;
    boolean onScroll = false;
    boolean pauseScroll = false;
    ObjectAnimator anim;
    double maxY;
    int _ctot,_cmaxY;

    View item_manage;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_page, container, false);
        init(root);
        return root;
    }

    void MediaInit(){
        mediaplayer = new MediaPlayer();
        mediaplayer.setAudioStreamType(AudioManager.STREAM_MUSIC);
    }

    void PlayOrPause(View imageButton,String audioUrl){
        if(mediaplayer != null &&!mediaplayer.isPlaying()) {
            try {

                mediaplayer.setDataSource(audioUrl);
                mediaplayer.prepare();


            } catch (IllegalArgumentException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (SecurityException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IllegalStateException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            mediaplayer.start();
        }else {
            assert mediaplayer != null;
            mediaplayer.stop();
        }
    }

    void NambahSpeed(){
        speed = new ArrayList<>();
        MyBPMSpeed speedLento = new MyBPMSpeed("Lento",40,66,17);
        speed.add(speedLento);
        MyBPMSpeed speedAdagio = new MyBPMSpeed("Adagio",66,80,15);
        speed.add(speedAdagio);
        MyBPMSpeed speedAndante = new MyBPMSpeed("Andante",76,108,10);
        speed.add(speedAndante);
        MyBPMSpeed speedModerato = new MyBPMSpeed("Moderato",108,120,8);
        speed.add(speedModerato);
        MyBPMSpeed speedAlegro = new MyBPMSpeed("Alegro",120,168,5);
        speed.add(speedAlegro);
    }
    void settingItemOnClick(int id){
        SavePerfsSeting(id);
        if(onScroll){
            PauseScrole();
            ResumeScrole();
        }
    }

    List<View> views;
    void ResetSetting(){
        view_speed.removeAllViews();
        views = new ArrayList<>();
        for (int a = 0; a<speed.size();a++){
            item_manage = LayoutInflater.from(getContext())
                    .inflate(R.layout.item_speed, view_speed, false);
            View ner = item_manage;
            views.add(ner);
            view_speed.addView(ner);
        }
        RecoloringSpeedSetting(getSaveIndexSpeed());
    }

    @SuppressLint("SetTextI18n")
    void RecoloringSpeedSetting(int index){
        for (int a = 0; a<views.size();a++){
            View ner = views.get(a);
            TextView name = ner.findViewById(R.id.sppedTv1);
            TextView info = ner.findViewById(R.id.info);
            LinearLayout btn = ner.findViewById(R.id.speed1);
            name.setText(speed.get(a).name);
            info.setText(speed.get(a).minBPM+" - "+speed.get(a).minBPM+" BPM");
            if(a==index){
                name.setTextColor(Color.parseColor("#fcb212"));
            }else {
                if(BasePageActivity.isDarkModeEnabled) name.setTextColor(Color.WHITE);
                else name.setTextColor(Color.parseColor("#FF000000"));
            }

            int finalA = a;
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    settingItemOnClick(finalA);
                    RecoloringSpeedSetting(finalA);
                }
            });
        }

    }

    boolean isSettingPopup = false;
    void ShowSettingScroller(){
        if(isSettingPopup){
            view_speed.removeAllViews();
            isSettingPopup = false;
        }else {
            ResetSetting();
            isSettingPopup = true;
        }
    }

    void init(View view){
        MediaInit();
        sizeFontP = getSaveFontSize();
        btn_scroll = view.findViewById(R.id.linearlayout_btn_scroll);
        tvbtn_scroll = view.findViewById(R.id.linearlayouttv_btn_scroll);
        tv_judul = view.findViewById(R.id.judul);
        linearLayout = view.findViewById(R.id.sam_text);
        scrollView = view.findViewById(R.id.page_scrol_view);
        scrollView.setOnScrollChangeListener(this);
        scrollView.setOnTouchListener(this);
        btn_seting = view.findViewById(R.id.settingSpeed_btn);
        btn_seting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowSettingScroller();
            }
        });
        view_speed = view.findViewById(R.id.view_speed);

        btn_scroll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(onScroll){
                    StopScrole();
                }else if(pauseScroll){
                    ResumeScrole();
                }else {
                    ScrollAuto();
                }
            }
        });
        tubePlayer = view.findViewById(R.id.youtube_player_view);

//        ytVid = view.findViewById(R.id.videoWebView);
//        ytVid.getSettings().setJavaScriptEnabled(true);
//        ytVid.setWebChromeClient(new WebChromeClient(){
//
//        });


        tUp = view.findViewById(R.id.btn_tranposeup);
        tUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TranUp();
            }
        });
        tDown = view.findViewById(R.id.btn_tranposedown);
        tDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                TranDown();
            }
        });
        fontUp = view.findViewById(R.id.btn_zoomup);
        fontUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SizeUpFont();
            }
        });
        fontDown = view.findViewById(R.id.btn_zoomdown);
        fontDown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SizeDownFont();
            }
        });
        tPlayMp3 = view.findViewById(R.id.btn_playmp3);
        tPlayMp3.setVisibility(View.GONE);


        new API(Info.URL + Info.LPage(BasePageActivity.valPage), new APIListener() {
            @Override
            public void failed(String err) {

            }

            @Override
            public void sucess(String v) {
                Lagu lagu = new Lagu(v);
                tv_judul.setText(new ToString().encodeHtml(lagu.judul));
                rawChordSave = new ToString().encodeHtmlChordRaw(lagu.chords);
                List<TextView> yun = new Tranposer().BeSpan(rawChordSave,getActivity(),sizeFontP);
                for (int y = 0;y<yun.size();y++){
                    linearLayout.addView(yun.get(y));
                }
                _cmaxY = yun.get(0).getLayoutParams().height;
                _ctot = yun.size();


                tubePlayer.enableBackgroundPlayback(false);
                tubePlayer.setEnableAutomaticInitialization(false);
                getLifecycle().addObserver(tubePlayer);
                tubePlayer.initialize(new YouTubePlayerListener() {
                    @Override
                    public void onReady(@NonNull YouTubePlayer youTubePlayer) {
                        youTubePlayer.cueVideo(lagu.link_youtube,0);
                    }

                    @Override
                    public void onStateChange(@NonNull YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlayerState playerState) {

                    }

                    @Override
                    public void onPlaybackQualityChange(@NonNull YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlaybackQuality playbackQuality) {

                    }

                    @Override
                    public void onPlaybackRateChange(@NonNull YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlaybackRate playbackRate) {

                    }

                    @Override
                    public void onError(@NonNull YouTubePlayer youTubePlayer, @NonNull PlayerConstants.PlayerError playerError) {

                    }

                    @Override
                    public void onCurrentSecond(@NonNull YouTubePlayer youTubePlayer, float v) {

                    }

                    @Override
                    public void onVideoDuration(@NonNull YouTubePlayer youTubePlayer, float v) {

                    }

                    @Override
                    public void onVideoLoadedFraction(@NonNull YouTubePlayer youTubePlayer, float v) {

                    }

                    @Override
                    public void onVideoId(@NonNull YouTubePlayer youTubePlayer, @NonNull String s) {

                    }

                    @Override
                    public void onApiChange(@NonNull YouTubePlayer youTubePlayer) {

                    }
                },true);
            }
        }).execute();

        NambahSpeed();
    }
    void TranUp(){
        rawChordSave = new Tranposer().Up(rawChordSave);
        linearLayout.removeAllViews();
        List<TextView> yun = new Tranposer().BeSpan(rawChordSave,getActivity(),sizeFontP);
        for (int y = 0;y<yun.size();y++){
            linearLayout.addView(yun.get(y));
        }
    }

    void TranDown(){
        rawChordSave = new Tranposer().Down(rawChordSave);
        linearLayout.removeAllViews();
        List<TextView> yun = new Tranposer().BeSpan(rawChordSave,getActivity(),sizeFontP);
        for (int y = 0;y<yun.size();y++){
            linearLayout.addView(yun.get(y));
        }
    }
    void SizeUpFont(){
        float newSize = getSaveFontSize()+1;
        if(newSize>23) return;

        linearLayout.removeAllViews();
        List<TextView> yun = new Tranposer().BeSpan(rawChordSave,getActivity(),newSize);
        for (int y = 0;y<yun.size();y++){
            linearLayout.addView(yun.get(y));
        }
        _cmaxY = yun.get(0).getLayoutParams().height;
        _ctot = yun.size();
        sizeFontP = newSize;
        SavePerfs(newSize);
    }

    void SizeDownFont(){
        float newSize = getSaveFontSize()-1;
        if(newSize<13) return;
        linearLayout.removeAllViews();
        List<TextView> yun = new Tranposer().BeSpan(rawChordSave,getActivity(),newSize);
        for (int y = 0;y<yun.size();y++){
            linearLayout.addView(yun.get(y));
        }
        _cmaxY = yun.get(0).getLayoutParams().height;
        _ctot = yun.size();
        sizeFontP = newSize;
        SavePerfs(newSize);
    }

    float getSaveFontSize(){
        final SharedPreferences preferences = getActivity().getSharedPreferences(PREFS_FILE,
                MODE_PRIVATE);
        return preferences.getFloat(REFERENCE_SIZE_FONT, 16);
    }

    int getSaveIndexSpeed(){
        final SharedPreferences preferences = getActivity().getSharedPreferences(PREFS_FILE,
                MODE_PRIVATE);
        return preferences.getInt(REFERENCE_SPEED, 0);
    }

    void SavePerfs(Float newVal){
        final SharedPreferences preferences = getActivity().getSharedPreferences(PREFS_FILE,
                MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putFloat(REFERENCE_SIZE_FONT, newVal);
        editor.apply();
    }
    void SavePerfsSeting(int newVal){
        final SharedPreferences preferences = getActivity().getSharedPreferences(PREFS_FILE,
                MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putInt(REFERENCE_SPEED, newVal);
        editor.apply();
    }
    int pointScrollChord = 666;
    int newPointScroll = 0;
    int nowPointScroll = 0;
    boolean doScroll = false;
    boolean _onScroll = false;
    boolean _onPScroll = false;

    List<MyBPMSpeed> speed = new ArrayList<>();

    @Override
    public void onScrollChange(View v, int scrollX, int scrollY, int oldScrollX, int oldScrollY) {
        nowPointScroll = scrollY;
        if(_onPScroll && scrollY<pointScrollChord){
            StopScrole();
        }else if(oldScrollY>=scrollView.getBottom()){

        }

        if(_onScroll&& scrollY==newPointScroll){
            doScroll = true;
        }else if(onScroll && scrollY==pointScrollChord&&!_onPScroll){
            doScroll = true;
        }else return;

        if(doScroll){
            _onScroll = true;
            doScroll = false;
            newPointScroll = scrollY + 450;
            anim = ObjectAnimator.ofInt(scrollView, "scrollY", newPointScroll);
            anim.setDuration(1000L *speed.get(getSaveIndexSpeed()).GetInSecond());
            anim.setInterpolator(new LinearInterpolator());
            anim.start();
        }
    }
    @Override
    public void onDetach() {
        super.onDetach();

        if(onScroll) {
            StopScrole();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if(onScroll) {
            StopScrole();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }
    @Override
    public boolean onTouch(View v, MotionEvent event) {
        if(v == scrollView){
            if(event.getAction()==MotionEvent.ACTION_MOVE){
                if(onScroll) PauseScrole();
            }
        }
        return false;
    }

    void ScrollAuto(){
        tvbtn_scroll.setText("Stop Scroll");
        onScroll = true;
        anim = ObjectAnimator.ofInt(scrollView, "scrollY", pointScrollChord);
        anim.setDuration(1000);
        anim.start();
    }
    void StopScrole(){
        tvbtn_scroll.setText("Auto Scroll");
        onScroll = false;
        _onScroll = false;
        pauseScroll = false;
        _onPScroll = false;
        anim.cancel();
    }
    void PauseScrole(){
        _onPScroll = true;
        tvbtn_scroll.setText("Resume Scroll");
        pauseScroll = true;
        onScroll = false;
        _onScroll = false;
        anim.cancel();
    }
    void ResumeScrole(){
        tvbtn_scroll.setText("Stop Scroll");
        onScroll = true;
        _onScroll = true;
        pauseScroll = false;
        doScroll = true;
        newPointScroll = nowPointScroll+10;
        anim = ObjectAnimator.ofInt(scrollView, "scrollY", (newPointScroll));
        anim.setInterpolator(new LinearInterpolator());
        anim.setDuration(100);
        anim.start();
    }


}