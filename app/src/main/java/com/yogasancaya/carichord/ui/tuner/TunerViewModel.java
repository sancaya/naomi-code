package com.yogasancaya.carichord.ui.tuner;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class TunerViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public TunerViewModel() {

    }

    public LiveData<String> getText() {
        return mText;
    }
}