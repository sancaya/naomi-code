package com.yogasancaya.carichord.ui.search;

import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.yogasancaya.carichord.BasePageActivity;
import com.yogasancaya.carichord.R;
import com.yogasancaya.carichord.api.API;
import com.yogasancaya.carichord.api.APIListener;
import com.yogasancaya.carichord.config.Info;
import com.yogasancaya.carichord.models.DataSearch;
import com.yogasancaya.carichord.models.DataSearchResult;

import java.util.ArrayList;

public class SearchFragment extends Fragment {
    private static final String TAG = "TEST";
    EditText textCari;
    ImageButton cariBtn;

    TextView tv_htHasil,tv_notice;
    RecyclerView hasilCari;
    HasilSearchAdapter searchAdapter;

    DataSearch dataSearch;

    boolean isLoading = false;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_search, container, false);
        init(root);
        return root;
    }

    void init(View view){
        tv_htHasil = view.findViewById(R.id.hasil_pencarian_text_head);
        tv_notice = view.findViewById(R.id.tv_hasil);
        tv_htHasil.setText(getString(R.string.hasil_pencarian_add_text)+": "+BasePageActivity.valQ);

        textCari = view.findViewById(R.id.cari_header_text);
        textCari.setText(BasePageActivity.valQ);
        cariBtn = view.findViewById(R.id.cari_header);

        cariBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(textCari.getText().toString().isEmpty()){
                    Toast.makeText(getActivity(), getString(R.string.toast_input_not_field), Toast.LENGTH_SHORT).show();
                    textCari.requestFocus();
                }else {
                    PushApi(textCari.getText().toString());
                }
            }
        });

        hasilCari = view.findViewById(R.id.rv_hasilCari);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        hasilCari.setLayoutManager(manager);

        new API(Info.URL + Info.QSearch(BasePageActivity.valQ), new APIListener() {
            @Override
            public void failed(String err) {
                tv_notice.setText("Error "+err);
            }

            @Override
            public void sucess(String v) {
                dataSearch = new DataSearch(v);
                tv_htHasil.setText(getString(R.string.hasil_pencarian_add_text)+": "+BasePageActivity.valQ+" ditemukan "+dataSearch.total);
                if(dataSearch.total == 0){
                    tv_notice.setText("Tidak ditemukan hasil");
                }else {
                    searchAdapter = new HasilSearchAdapter(dataSearch, new SearchClick() {
                        @Override
                        public void OnClick(int a,DataSearchResult searchResult) {
                            ((BasePageActivity) getActivity()).SetFragment(R.id.nav_page, getActivity(), searchResult.lagu);
                        }
                    });
                    hasilCari.setAdapter(searchAdapter);
                    searchAdapter.notifyDataSetChanged();
                    tv_notice.setVisibility(View.GONE);
                    initScrollListener();
                }
            }
        }).execute();

    }

    private void initScrollListener() {
        hasilCari.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if(maxresult())return;
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == dataSearch.results.size() - 1) {
                        //bottom of list!
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }
    boolean maxresult(){
        return dataSearch.total-(dataSearch.page*dataSearch.results.size())==0;
    }
    private void loadMore() {
        dataSearch.results.add(null);
        searchAdapter.notifyItemInserted(dataSearch.results.size() - 1);
        new API(Info.URL + Info.QSearch(BasePageActivity.valQ,dataSearch.page+1), new APIListener() {
            @Override
            public void failed(String err) {

            }

            @Override
            public void sucess(String v) {

                dataSearch.results.remove(dataSearch.results.size() - 1);
                int scrollPosition = dataSearch.results.size();
                searchAdapter.notifyItemRemoved(scrollPosition);

                DataSearch _dataSearch = new DataSearch(v);
                dataSearch.total = _dataSearch.total;
                dataSearch.totalpage = _dataSearch.totalpage;
                dataSearch.page = _dataSearch.page;
                dataSearch.results.addAll(_dataSearch.results);


                searchAdapter.notifyDataSetChanged();
                isLoading = false;
            }
        }).execute();
    }

    void PushApi(String query){
        ((BasePageActivity)getActivity()).hideKeyboard(getActivity());
        tv_notice.setVisibility(View.VISIBLE);
        BasePageActivity.valQ = query;
        tv_notice.setText("connecting");
        tv_htHasil.setText(getString(R.string.hasil_pencarian_add_text)+": "+query);

        new API(Info.URL + Info.QSearch(query), new APIListener() {
            @Override
            public void failed(String err) {
                tv_notice.setText("Error "+err);
            }

            @Override
            public void sucess(String v) {

                dataSearch = new DataSearch(v);
                if(dataSearch.total == 0){
                    tv_notice.setText("Tidak ditemukan hasil");
                }else {
                    searchAdapter = new HasilSearchAdapter(dataSearch, new SearchClick() {
                        @Override
                        public void OnClick(int a,DataSearchResult searchResult) {
                            ((BasePageActivity) getActivity()).SetFragment(R.id.nav_page, getActivity(), searchResult.lagu);
                        }
                    });
                    hasilCari.setAdapter(searchAdapter);
                    searchAdapter.notifyDataSetChanged();
                    tv_notice.setVisibility(View.GONE);
                }
            }
        }).execute();
    }
}