package com.yogasancaya.carichord.ui.search;

import com.yogasancaya.carichord.models.DataSearch;
import com.yogasancaya.carichord.models.DataSearchResult;

public interface SearchClick {
    void OnClick(int pos,DataSearchResult searchResult);
}
