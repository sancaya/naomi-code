package com.yogasancaya.carichord.ui.artistpage;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;
import com.yogasancaya.carichord.BasePageActivity;
import com.yogasancaya.carichord.R;
import com.yogasancaya.carichord.api.API;
import com.yogasancaya.carichord.api.APIListener;
import com.yogasancaya.carichord.api.NyolongGambarGoogleAPI;
import com.yogasancaya.carichord.api.NyolongGoggleListener;
import com.yogasancaya.carichord.config.Info;
import com.yogasancaya.carichord.models.DataArtist;
import com.yogasancaya.carichord.models.DataSearchResult;
import com.yogasancaya.carichord.models.SingleArtisData;
import com.yogasancaya.carichord.models.SingleArtisDataResult;
import com.yogasancaya.carichord.ui.request.RequestViewModel;
import com.yogasancaya.carichord.ui.search.HasilSearchAdapter;
import com.yogasancaya.carichord.ui.search.SearchClick;

import java.util.List;

public class ArtistPageFragment extends Fragment {

    private RequestViewModel requestViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        requestViewModel =
                new ViewModelProvider(this).get(RequestViewModel.class);
        View root = inflater.inflate(R.layout.fragment_artist_page, container, false);
        init(root);
        return root;
    }
    ImageView photo;
    TextView namaArtis;
    RecyclerView hasilCari;
    ArtisPageAdapter searchAdapter;

    SingleArtisData dataSearch;

    boolean isLoading = false;

    void init(View view){
        photo = view.findViewById(R.id.photo);
        namaArtis = view.findViewById(R.id.nama_artis);
        hasilCari = view.findViewById(R.id.artis_recycle_album);

        namaArtis.setText("menghub...");

        RecyclerView.LayoutManager manager = new GridLayoutManager(getContext(),2);
        hasilCari.setLayoutManager(manager);

        new API(Info.URL + Info.Artist(BasePageActivity.valPageArtis), new APIListener() {
            @Override
            public void failed(String err) {

            }

            @Override
            public void sucess(String v) {
                SingleArtisData artisData = new SingleArtisData(v);
                dataSearch = artisData;

                new NyolongGambarGoogleAPI(BasePageActivity.valPageArtis, new NyolongGoggleListener() {
                    @Override
                    public void Result(List<String> strings) {
                        Picasso.with(getContext()).load(strings.get(getRandomNumber(0,strings.size()))).fit().into(photo);
                        namaArtis.setText(artisData.results.get(0).artis);

                        searchAdapter = new ArtisPageAdapter(artisData, new ArtisAlbumClick() {
                            @Override
                            public void OnClick(int pos, SingleArtisDataResult searchResult) {
                                ((BasePageActivity) getActivity()).SetFragment(R.id.nav_page, getActivity(), searchResult.lagu);
                            }
                        },getActivity(),strings);

                        hasilCari.setAdapter(searchAdapter);
                        searchAdapter.notifyDataSetChanged();
                        initScrollListener();
                    }
                }).execute();


            }
        }).execute();
    }

    public int getRandomNumber(int min, int max) {
        return (int) ((Math.random() * (max - min)) + min);
    }

    private void initScrollListener() {

        hasilCari.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView rView, int newState) {

                super.onScrollStateChanged(rView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if(maxresult())return;
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == dataSearch.results.size() - 1) {
                        //bottom of list!
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }
    boolean maxresult(){
        return dataSearch.total-(dataSearch.page)==0;
    }
    private void loadMore() {
        dataSearch.results.add(null);
        searchAdapter.notifyItemInserted(dataSearch.results.size() - 1);
            new API(Info.URL + Info.Artist(BasePageActivity.valPageArtis,dataSearch.page + 1), new APIListener() {
                @Override
                public void failed(String err) {

                }

                @Override
                public void sucess(String v) {

                    dataSearch.results.remove(dataSearch.results.size() - 1);
                    int scrollPosition = dataSearch.results.size();
                    searchAdapter.notifyItemRemoved(scrollPosition);

                    SingleArtisData _dataSearch = new SingleArtisData(v);
                    dataSearch.total = _dataSearch.total;
                    dataSearch.totalpage = _dataSearch.totalpage;
                    dataSearch.page = _dataSearch.page;
                    dataSearch.results.addAll(_dataSearch.results);


                    searchAdapter.notifyDataSetChanged();
                    isLoading = false;
                }
            }).execute();
    }
}