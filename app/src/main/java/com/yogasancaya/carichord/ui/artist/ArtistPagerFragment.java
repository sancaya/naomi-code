package com.yogasancaya.carichord.ui.artist;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.yogasancaya.carichord.BasePageActivity;
import com.yogasancaya.carichord.R;
import com.yogasancaya.carichord.api.API;
import com.yogasancaya.carichord.api.APIListener;
import com.yogasancaya.carichord.config.Info;
import com.yogasancaya.carichord.models.DataArtisResult;
import com.yogasancaya.carichord.models.DataArtist;

public class ArtistPagerFragment extends Fragment {
    public static final String ARG_OBJECT = "object";
    ArtistPagerAdapter artistPagerAdapter;
    ViewPager2 viewPager2;

    RecyclerView recyclerView;
    TextView tv_htHasil,tv_notice;

    DataArtist dataSearch;
    ArtistAdapter adapter;

    boolean isLoading = false;

    int index;
    String key;

    public ArtistPagerFragment(){

    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_artist_collections, container, false);
        Bundle args = getArguments();
        assert args != null;
        index = args.getInt("id");
        key = args.getString("key");

        init(root);
        return root;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        Bundle args = getArguments();
        assert args != null;
        index = args.getInt("id");
        key = args.getString("key");

    }
    void init(View view){
        tv_notice = view.findViewById(R.id.tv_hasil);

        recyclerView = view.findViewById(R.id.frag_artis);
        RecyclerView.LayoutManager manager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(manager);
        if(key==null) {
            new API(Info.URL + Info.ArtistTrend(), new APIListener() {
                @Override
                public void failed(String err) {
                    tv_notice.setText("Error " + err);
                }

                @Override
                public void sucess(String v) {
                    dataSearch = new DataArtist(v);
                    if (dataSearch.total == 0) {
                        tv_notice.setText("Tidak ditemukan hasil");
                    } else {
                        adapter = new ArtistAdapter(dataSearch, new ArtisItemClick() {
                            @Override
                            public void OnClick(int pos, DataArtisResult searchResult) {
                                ((BasePageActivity)getActivity()).SetFragmentArtistPage(searchResult.getArtisurl());
                            }
                        });
                        recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        tv_notice.setVisibility(View.GONE);
                        initScrollListener();
                    }
                }
            }).execute();
        }else {
            new API(Info.URL + Info.ArtistTrend(key), new APIListener() {
                @Override
                public void failed(String err) {
                    tv_notice.setText("Error " + err);
                }

                @Override
                public void sucess(String v) {
                    dataSearch = new DataArtist(v);
                    if (dataSearch.total == 0) {
                        tv_notice.setText("Tidak ditemukan hasil");
                    } else {
                        adapter = new ArtistAdapter(dataSearch, new ArtisItemClick() {
                            @Override
                            public void OnClick(int pos, DataArtisResult searchResult) {

                            }
                        });
                        recyclerView.setAdapter(adapter);
                        adapter.notifyDataSetChanged();
                        tv_notice.setVisibility(View.GONE);
                        initScrollListener();
                    }
                }
            }).execute();
        }
    }
    private void initScrollListener() {

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView rView, int newState) {

                super.onScrollStateChanged(rView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();

                if (!isLoading) {
                    if(maxresult())return;
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == dataSearch.results.size() - 1) {
                        //bottom of list!
                        loadMore();
                        isLoading = true;
                    }
                }
            }
        });
    }
    boolean maxresult(){
        return dataSearch.total-(dataSearch.page)==0;
    }
    private void loadMore() {
        dataSearch.results.add(null);
        adapter.notifyItemInserted(dataSearch.results.size() - 1);
        if(key==null) {
            new API(Info.URL + Info.ArtistTrend(dataSearch.page + 1), new APIListener() {
                @Override
                public void failed(String err) {

                }

                @Override
                public void sucess(String v) {

                    dataSearch.results.remove(dataSearch.results.size() - 1);
                    int scrollPosition = dataSearch.results.size();
                    adapter.notifyItemRemoved(scrollPosition);

                    DataArtist _dataSearch = new DataArtist(v);
                    dataSearch.total = _dataSearch.total;
                    dataSearch.totalpage = _dataSearch.totalpage;
                    dataSearch.page = _dataSearch.page;
                    dataSearch.results.addAll(_dataSearch.results);


                    adapter.notifyDataSetChanged();
                    isLoading = false;
                }
            }).execute();
        }else {
            new API(Info.URL + Info.ArtistTrend(dataSearch.page + 1,key), new APIListener() {
                @Override
                public void failed(String err) {

                }

                @Override
                public void sucess(String v) {

                    dataSearch.results.remove(dataSearch.results.size() - 1);
                    int scrollPosition = dataSearch.results.size();
                    adapter.notifyItemRemoved(scrollPosition);

                    DataArtist _dataSearch = new DataArtist(v);
                    dataSearch.total = _dataSearch.total;
                    dataSearch.totalpage = _dataSearch.totalpage;
                    dataSearch.page = _dataSearch.page;
                    dataSearch.results.addAll(_dataSearch.results);


                    adapter.notifyDataSetChanged();
                    isLoading = false;
                }
            }).execute();
        }
    }

    public static ArtistPagerFragment newInstance(int id,String key) {
        Bundle args = new Bundle();
        args.putString("key", key);
        args.putInt("id", id);
        ArtistPagerFragment f = new ArtistPagerFragment();
        f.setArguments(args);
        return f;
    }
}
