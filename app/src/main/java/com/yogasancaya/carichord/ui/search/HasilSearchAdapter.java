package com.yogasancaya.carichord.ui.search;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.yogasancaya.carichord.R;
import com.yogasancaya.carichord.models.DataSearch;
import com.yogasancaya.carichord.utils.ToString;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;


public class HasilSearchAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;

    DataSearch dataSearch;
    SearchClick searchClick;
    public HasilSearchAdapter(DataSearch dataSearch){
        this.dataSearch = dataSearch;
    }
    public HasilSearchAdapter(DataSearch dataSearch,SearchClick searchClick){
        this.dataSearch = dataSearch;
        this.searchClick = searchClick;
    }

    public void AddData(DataSearch dataSearch){
        this.dataSearch.results.addAll(dataSearch.results);
    }

    @Override
    public int getItemViewType(int position) {
        return dataSearch.results.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_chord_terbaru, parent, false);
            return new ItemViewHolder(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_lead_more, parent, false);
            return new LoadingViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ItemViewHolder) {
            populateItemRows((ItemViewHolder) holder, position);
        } else if (holder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) holder, position);
        }
    }
    String ytImage(String ytId){
        return "https://img.youtube.com/vi/"+ytId+"/default.jpg";
    }
    public static Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    @Override
    public int getItemCount() {
        return dataSearch.results == null? 0 :dataSearch.results.size();
    }

    void populateItemRows(ItemViewHolder holder,int position){
        holder.title.setText(new ToString().encodeHtml(dataSearch.results.get(position).judul));
        holder.date.setText(new ToString().encodeDate(dataSearch.results.get(position).date));
        holder.views.setText(dataSearch.results.get(position).view);
//        holder.thumbnail.setImageBitmap(getBitmapFromURL(ytImage(dataSearch.results.get(position).artisurl)));
        holder.item.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(searchClick!=null) searchClick.OnClick(holder.getAdapterPosition(),dataSearch.results.get(position));
            }
        });
    }
    void showLoadingView(LoadingViewHolder holder,int position){
        //TODO Loading Adapter Search
    }

    private class ItemViewHolder extends RecyclerView.ViewHolder {

        public TextView title;
        public TextView date;
        public TextView views;
        public View item;
        public ImageView thumbnail;
        public ItemViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.title);
            date = (TextView) view.findViewById(R.id.date);
            views = (TextView) view.findViewById(R.id.view);
            thumbnail = (ImageView) view.findViewById(R.id.image_thumbnail);
            // Define click listener for the ViewHolder's View
            item = view;
        }
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }



}
