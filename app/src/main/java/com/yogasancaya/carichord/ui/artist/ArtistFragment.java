package com.yogasancaya.carichord.ui.artist;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Lifecycle;
import androidx.viewpager2.widget.ViewPager2;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;
import com.yogasancaya.carichord.BasePageActivity;
import com.yogasancaya.carichord.R;

import java.util.ArrayList;
import java.util.List;

public class ArtistFragment extends Fragment {

    ViewPager2 viewPager;
    TabLayout tabLayout;
    List<String> tabs;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_artist, container, false);

        init(root);
        return root;
    }
    void init(View view){
        validate(view);
        viewPager = view.findViewById(R.id.pager);
        tabLayout = view.findViewById(R.id.tab_layout);
        tabsGenerator(tabLayout);
    }
    void validate(View view){
        tabs = new ArrayList<>();
        tabs.add("Popular");
        tabs.add("A");
        tabs.add("B");
        tabs.add("C");
        tabs.add("D");
        tabs.add("E");
        tabs.add("F");
        tabs.add("G");
        tabs.add("H");
        tabs.add("I");
        tabs.add("J");
        tabs.add("K");
        tabs.add("L");
        tabs.add("M");
        tabs.add("N");
        tabs.add("O");
        tabs.add("P");
        tabs.add("Q");
        tabs.add("R");
        tabs.add("S");
        tabs.add("T");
        tabs.add("U");
        tabs.add("V");
        tabs.add("W");
        tabs.add("X");
        tabs.add("Y");
        tabs.add("Z");
    }

    void tabsGenerator(TabLayout tabLayout){
        for (int u = 0;u<tabs.size();u++){
            tabLayout.addTab(tabLayout.newTab().setText(tabs.get(u)));
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
        ArtistPagerAdapter adapter = new ArtistPagerAdapter(getActivity() ,tabs, getLifecycle());
        viewPager.setAdapter(adapter);
        new TabLayoutMediator(tabLayout, viewPager, new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                tab.setText(tabs.get(position));
            }
        }).attach();
    }

}