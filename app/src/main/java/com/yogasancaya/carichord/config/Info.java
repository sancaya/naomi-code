package com.yogasancaya.carichord.config;

import android.util.Log;

public class Info {
    public static final String DEVELOPER_KEY = "AIzaSyBnJOepD3zS_brNFGZdcqFLjgqTm8LBrvk";

    public static String URL = "https://carichord.com/";

    public static String QSearch(String val){
        return "search?q="+val+"&type=json";
    }
    public static String QSearch(String val,int page){
        return "search?p="+page+"&q="+val+"&type=json";
    }
//    https://carichord.com/artist-band?&type=json
//    https://carichord.com/artist-band?a=a&type=json
    public static String ArtistTrend(){
        return "artist-band?&type=json";
    }
    public static String ArtistTrend(int page){
        Log.d("TAG", ": "+page);
        return "artist-band?p="+page+"&type=json";
    }
    public static String ArtistTrend(String key){
        return "artist-band?a="+key+"&type=json";
    }
    public static String ArtistTrend(int page,String key){
        Log.d("TAG", ": "+page);
        return "artist-band?p="+page+"&a="+key+"&type=json";
    }
    public static String LPage(String val){
        return "chord.php?lagu="+val+"&type=json";
    }
//    https://carichord.com/view-artist?name=baby-shima&type=json
    public static String Artist(String nameLink){
        return "view-artist?name="+nameLink+"&type=json";
    }
    public static String Artist(String nameLink,int p){
        return "view-artist?p="+p+"name="+nameLink+"&type=json";
    }
}
